import { MyService } from './services/my.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// para el binding y los forms
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { RegisterComponent } from './register/register.component';
import { MainComponent } from './main/main.component';


import { RouterModule, Routes} from '@angular/router';
import { NotaComponent } from './nota/nota.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { UserService } from './services/user.service';
import { HttpModule } from '@angular/http';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './guards/auth.guard';
import { SafeURLPipe } from './pipes/safe-url.pipe';
import { NoteService } from './services/note.service';
import { NotebookService } from './services/notebook.service';


const appRoutes = [
  {path:'inicio', canActivate:[AuthGuard], component: MainComponent},
  {path:'register', component: RegisterComponent},
  {path:'signin', component: SignInComponent},
  {path: '', canActivate:[AuthGuard], redirectTo: '/inicio', pathMatch: 'full'},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    RegisterComponent,
    MainComponent,
    NotaComponent,
    SignInComponent,
    SafeURLPipe,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false}
    ),
    FormsModule,
    HttpModule,
  ],
  providers: [
    MyService,
    FormsModule,
    HttpModule,
    UserService,
    AuthService,
    NoteService,
    NotebookService,
    AuthGuard,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
