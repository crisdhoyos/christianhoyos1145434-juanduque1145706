
import { MyService } from './../services/my.service';
import { Nota } from './../models/nota';
import { Libreta } from './../models/libreta';

declare var jquery: any;
declare var $: any;


import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { Notebook } from '../models/notebook';
import { Note } from '../models/note';
import { NotebookService } from '../services/notebook.service';
import { NoteService } from '../services/note.service';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  currentUserNotebooks : Notebook[] = null;
  currentUserNotes : Note[] = null;
  currentUser: any;
  nombreNuevaLibreta: any;
  nombreNuevaNota : any;
  idNoteModify : any;
  nombreLibretaModificada : any;
  idLibretaModificada : any;
  photo : any;
  constructor(private authService : AuthService, private router : Router, private notebookService : NotebookService, private noteService : NoteService) { 
    this.updateVars();
  }


  ngOnInit() {
    /*
    this.libretas = this.servicio.getLibretas();
    this.temporal = "";
    */
    this.nombreNuevaNota = "";
    this.nombreNuevaLibreta = "";

  }

  updateVars(){
    this.authService.getCurrentUserNotebooks().subscribe(res =>{
      this.currentUserNotebooks = res[1];
      if(res[1] != undefined || res[1] != null){
        for(let notebook of this.currentUserNotebooks){
          notebook.notes = [];
          this.noteService.getNotes('notebook_id='+notebook.id).subscribe(res=>{
            if(JSON.parse(res.text())[0] == '0'){
              notebook.notes.push(JSON.parse(res.text())[1]);
              //console.log(notebook.notes);
            }
          });
        }
      }
    });
    if(this.currentUserNotebooks != undefined || this.currentUserNotebooks != null){
      this.authService.getCurrentUserNotes(this.authService.getCurrentUser().id).subscribe(res =>{
        this.currentUserNotes = res[1];
      });
    }
    this.currentUser = this.authService.getCurrentUser();
    var reader = new FileReader();
    var that = this;
    console.log(this.currentUser.photo)
    that.photo = 'data:image/jpeg;base64,'+btoa(this.currentUser.photo);
  }

  contraerLibreta(evt){
    let id = evt.target.id;
    if (document.getElementById("Libreta" + id).style.display == "none"){
      document.getElementById("Libreta" + id).style.display = "block";
    }else{
      document.getElementById("Libreta" + id).style.display = "none";
    }
  }
  
  aceptarBorrar(){
    let elemento= document.getElementById("nombreElemento").innerHTML;
    let valor= document.getElementById("idElemento").innerHTML;
    if (elemento == "libreta") {
      this.notebookService.deleteNotebook(valor)
      .subscribe(val => {
        console.log(val);
        this.updateVars();
      });
    }
    if (elemento == "nota") {
      this.noteService.deleteNotes(valor).subscribe(res => {
        console.log(res);
        this.updateVars();
      });
    }
    document.getElementById("nombreElemento").innerHTML = "";
    document.getElementById("idElemento").innerHTML = "";

    document.getElementById("dialog-confirm").style.display = "none";
  }
  
  
  cancelarBorrar(){
    document.getElementById("nombreElemento").innerHTML = "";
    document.getElementById("idElemento").innerHTML = "";
  
    document.getElementById("dialog-confirm").style.display = "none";

  }
  
  eliminarNota(evt){
    let idNota = evt.target.id;
    document.getElementById("nombreElemento").innerHTML = "nota";
    document.getElementById("idElemento").innerHTML = idNota;
    document.getElementById("dialog-confirm").style.display = "flex";
    
    //para mostrar el titulo de la nota en el pop up
    this.currentUserNotebooks.forEach(libreta => {
      let aux  = libreta.notes[0];
      console.log(aux);
      if(aux != undefined){
        aux.forEach(nota => {
          if (nota.id == idNota){
            document.getElementById("tituloElemento").innerHTML = nota.title;
          }
        });
      }
    });
  }
  
  eliminarLibreta(evt){
    let idLibreta = evt.target.id;
    document.getElementById("nombreElemento").innerHTML = "libreta";
    document.getElementById("idElemento").innerHTML = idLibreta;
    document.getElementById("dialog-confirm").style.display = "flex";

    this.currentUserNotebooks.forEach(libreta => {
      if (libreta["id"] == idLibreta) {
        document.getElementById("tituloElemento").innerHTML = libreta["name"];
      }
    });
  }

  cerrarCrearNota(){
    document.getElementById("creadorDeNotas").style.display = "none";
    document.getElementById("libretaDeLaNuevaNota").innerHTML = "";
    this.nombreNuevaNota = "";
  }

  cerrarCrearLibreta(){
    document.getElementById("creadorDeLibretas").style.display = "none";
    this.nombreNuevaNota = "";
  }
  cerrarModificarLibreta(){
    document.getElementById("modificadorLibreta").style.display = "none";
    this.nombreLibretaModificada = "";
    this.idLibretaModificada = "";
  }

  activarCrearNota(evt){
    this.cerrarCrearLibreta();
    let idLibreta = evt.target.id;
    document.getElementById("creadorDeNotas").style.display = "flex";
    document.getElementById("libretaDeLaNuevaNota").innerHTML = idLibreta;

    document.getElementById("nombreNuevaNota").focus();
  }
  activarModificarLibreta(evt){
    this.cerrarCrearLibreta();
    let idLibreta = evt.target.id;
    document.getElementById("modificadorLibreta").style.display = "flex";
    this.idLibretaModificada = idLibreta;

    document.getElementById("nombreNuevaNota").focus();
  }

  activarCrearLibreta(evt){
    this.cerrarCrearNota();
    document.getElementById("creadorDeLibretas").style.display = "flex";

    document.getElementById("nombreNuevaLibreta").focus();
  }

  crearNota(evt){
    if (this.nombreNuevaNota == "") {
      alert("Introduzca un nombre");
    }else{
      document.getElementById("creadorDeNotas").style.display = "none";
      let idLibreta = document.getElementById("libretaDeLaNuevaNota").innerHTML;
      this.noteService.addNote(this.nombreNuevaNota, Date.now(), '', idLibreta)
      .subscribe(val => {
        console.log(val);
        this.updateVars();
      });
      document.getElementById("libretaDeLaNuevaNota").innerHTML = "";
  
      this.nombreNuevaNota = "";
    }
  }

  modificarLibreta(evt){
    if (this.nombreLibretaModificada == "") {
      alert("Introduzca un nombre");
    }else{
      document.getElementById("modificadorLibreta").style.display = "none";
      console.log("evento:",this.nombreLibretaModificada, this.idLibretaModificada);
      this.notebookService.updateNotebook(this.idLibretaModificada, this.nombreLibretaModificada).subscribe(res=>{
        console.log(res);
        this.updateVars();
        this.idLibretaModificada = "";
        this.nombreLibretaModificada = "";
      });
      // let idLibreta = document.getElementById("libretaDeLaNuevaNota").innerHTML;
      // this.noteService.addNote(this.nombreNuevaNota, Date.now(), '', idLibreta)
      // .subscribe(val => {
      //   console.log(val);
      //   this.updateVars();
      // });
      // document.getElementById("libretaDeLaNuevaNota").innerHTML = "";
  
      // this.nombreNuevaNota = "";
    }
  }
  
  crearLibreta(){
    if (this.nombreNuevaLibreta == "") {
      alert("Introduzca un nombre");
    } else {
      document.getElementById("creadorDeLibretas").style.display = "none";
      this.notebookService.addNotebook(this.nombreNuevaLibreta, Date.now(), this.authService.getCurrentUser().id)
      .subscribe(val => {
        console.log(val);
        this.updateVars();
      });

      this.nombreNuevaLibreta = "";
    }
  }

  mostrarNota(evt){
    this.idNoteModify = evt.target.id;
  }

}
