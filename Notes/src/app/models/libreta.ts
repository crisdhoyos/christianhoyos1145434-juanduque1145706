import { Nota } from './nota';

export class Libreta {

    id: number;
    date: number;
    name: string;
    notes: Nota[];

    constructor(id: number, date: number, name: string, notes?: Nota[]) {
        this.id = id;
        this.date = date;
        this.name = name;
        this.notes = notes;
    }
}