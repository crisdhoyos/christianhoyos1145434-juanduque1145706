export class Note {

    id : number;
    date: number;
    title: string;
    content: string;

    constructor(id: number, date: number, title: string, content: string) {
        this.id = id;
        this.date = date;
        this.title = title;
        this.content = content;
    }
    

}