import { Note } from "./note";

export class Notebook {

    id : number;
    date: number;
    name: string;
    notes : any;

    constructor(id : number, date: number, name: string, notes : Note[]) {
        this.id = id;
        this.date = date;
        this.name = name;
        this.notes = notes;
    }
}