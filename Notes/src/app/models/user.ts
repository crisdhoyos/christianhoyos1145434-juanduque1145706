import { Note } from "./note";
import { Notebook } from "./notebook";

export class User {

    id : number;
    username: number;
    password: string;
    email: string;
    photo: string;
    notes : Note[];
    sharedNotes : {
        note: Note,
        permission : number
    }[];
    sharedNotebooks : {
        notebook: Notebook,
        permission : number
    }[];

    constructor(id : number, username: number, password: string, email: string, photo: string, notes? : Note[],
        sharedNotes?:{note: Note,permission : number}[], sharedNotebooks? : {notebook: Notebook,permission : number}[]) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.photo = photo;
        this.notes = notes;
        this.sharedNotes = sharedNotes;
        this.sharedNotebooks = sharedNotebooks;
    }
    
}