import { Libreta } from './libreta';
import { Nota } from './nota';


export class Usuario {

    id: number;
    username: string;
    password: string;
    email: string;
    photo: string;
    notebooks: Libreta[];
    sharedNotes: {
        note: Nota,
        permission: number
    }[];
    sharedNotebooks: {
        notebook: Libreta,
        permission: number
    }[];

    constructor(id: number, username: string, password: string, email: string, photo: string, 
        notebooks?: Libreta[],
        sharedNotes?: { note: Nota, permission: number }[], 
        sharedNotebooks?: { notebook: Libreta, permission: number }[]) {

        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.photo = photo;
        this.notebooks = notebooks;
        this.sharedNotes = sharedNotes;
        this.sharedNotebooks = sharedNotebooks;
    }

}