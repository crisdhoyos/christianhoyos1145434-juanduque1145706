import { NoteService } from './../services/note.service';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-nota',
  templateUrl: './nota.component.html',
  styleUrls: ['./nota.component.scss']
})
export class NotaComponent implements OnInit {

  @Input()
  id:number;

  firstTime:boolean;

  titulo:string;
  contenido:string;
  fecha:string;
  mostrarNota : boolean;


  constructor(private noteService:NoteService) { 
  }

  ngOnInit() {
    this.firstTime= true;
    this.id = -1;
    this.titulo = "";
    this.fecha = "";
    this.contenido = "";
    this.mostrarNota = false;
  }

  ngOnChanges(changes) {
    this.noteService.getNotes('id=' + this.id).map(res =>{
      return res;
    })
    .subscribe(res => {
      try {
        console.log(JSON.parse(res.text())[1][0]);
        let respuesta = JSON.parse(res.text())[1][0];
        this.id = respuesta.id;
        this.titulo = respuesta.title;
        this.fecha = new Date(parseInt(respuesta.date)).toString().split('GMT')[0];

        // this.fecha = new Date(respuesta.date).getMonth().toString();
        this.contenido = respuesta.content;
        // this.id = respuesta.id;
        // this.titulo = respuesta.title;
        // this.fecha = respuesta.date;
        // this.contenido = respuesta.content;
        this.mostrarNota = true;
      } catch (error) {
        
      }
      
    });
  }

  updateValues(){
    this.noteService.updateNotes(this.id, this.titulo, this.contenido).subscribe(res=>{
      console.log(res);
      
    });
  }

}
