import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  constructor(private userService: UserService, private authService : AuthService, private router : Router) { 
    if(this.authService.getUserLogged()){
      this.router.navigate(['inicio']);
    } 
   }

  ngOnInit() {
  }

}
