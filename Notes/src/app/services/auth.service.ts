import { Injectable } from '@angular/core';
import { User } from '../models/User';
import { NgForm } from '@angular/forms';
import { UserService } from './user.service';

import "rxjs/Rx";
import { Note } from '../models/note';
import { Notebook } from '../models/notebook';
import { NoteService } from './note.service';
import { NotebookService } from './notebook.service';

@Injectable()
export class AuthService {

  private userLogged: boolean;
  private currentUser: User;
  constructor(private userService: UserService, private noteService: NoteService, private notebookService: NotebookService) {
    this.userLogged = false;
    this.currentUser = null;
    this.getCurrentUser();
  }

  getCurrentUser() {

    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    return this.currentUser;
  }

  getCurrentUserNotebooks(){
    let currentUserNotebooks: any;
    let notebookQuery = "user_id=" + this.currentUser.id;
    return this.notebookService.getNotebooks(notebookQuery).map(res =>{
      return JSON.parse(res.text());
    })
  }

  getCurrentUserNotes(id : number){
    let currentUserNotes: any;
    let noteQuery = "user_id=" + id;
    return this.noteService.getNotes(noteQuery).map(res=>{
      return res;
    });
  }

  getUserLogged(): boolean {
    this.userLogged = (localStorage.getItem('isLogged') == 'true');
    return this.userLogged;
  }

  signIn(form: NgForm) {
    return new Promise((resolve, reject) => {
      if (!this.userLogged) {
        let jsonUsers = null;
        this.userService.getUsers(form.value.username)
          .map(res => res)
          .subscribe(res => {
            jsonUsers = JSON.parse(res.text());
            if (jsonUsers[0] == 0) {
              if (form.value.password == jsonUsers[1][0].password) {
                console.log('Sesión iniciada');
                this.userLogged = true;
                this.currentUser = new User(jsonUsers[1][0].id, jsonUsers[1][0].username, jsonUsers[1][0].password, jsonUsers[1][0].email, jsonUsers[1][0].photo);
                localStorage.setItem('currentUser', JSON.stringify(this.currentUser));
                localStorage.setItem('isLogged', 'true');
                resolve(0);
              } else {
                console.log('Contraseña no válida');
                resolve(1);
              }
            } else {
              console.log('Usuario no existe');
              resolve(2);
            }
          });
      } else {
        console.log('Usuario ya ingresado');
        resolve(3);
      }

    });
  }

  signOut(evt){
    this.currentUser = null;
    this.userLogged = false;
    localStorage.removeItem('currentUser');
    localStorage.setItem('isLogged', 'false');
  }

}
