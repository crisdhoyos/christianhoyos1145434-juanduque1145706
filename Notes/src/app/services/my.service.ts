import { Usuario } from './../models/usuario';
import { Nota } from './../models/nota';
import { Libreta } from './../models/libreta';

import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import "rxjs";


@Injectable()
export class MyService {

  usuario: Usuario;
  libretas:Libreta[];
  notas:Nota[];

  constructor() {

    this.usuario= new Usuario(1,"admin","admin","crisdhoyos@gmail.com","image");

    this.notas = [
      new Nota(1,Date.now(),"Nota 1","Contenido Nota 1"),
      new Nota(2,Date.now(),"Nota 2","Contenido Nota 2"),
      new Nota(3,Date.now(),"Nota 3","Contenido Nota 3"),
      new Nota(4,Date.now(),"Nota 4","Contenido Nota 4")
    ];

    this.libretas = [
      new Libreta(1, Date.now(), "Libreta 1", [this.notas[0], this.notas[1]]),
      new Libreta(2, Date.now(), "Libreta 2", [this.notas[2], this.notas[3]])
    ]

  }

  getLibretas() {
    return this.libretas;
  }
  getNotas() {
    return this.notas;
  }

  setNota(posicion,contenido) {
    
  }

  eliminarNota(id) {
    this.libretas.forEach(libretica => {
      for (var i = libretica["notes"].length - 1; i >= 0; i--) {
        if (libretica["notes"][i]["id"] == id) {
          libretica["notes"].splice(i, 1);
          console.log("se elimino nota:", id);
        }
      }
    });
  }
  eliminarLibreta(id) {
    for (var i = this.libretas.length - 1; i >= 0; i--) {
      if (this.libretas[i]["id"] == id) {
        this.libretas.splice(i, 1);
        console.log("se elimino libreta:", id);
      }
    }
  }

  crearNota(idLibreta, nombre){
    this.libretas.forEach(libreta => {
      if (libreta["id"] == idLibreta) {
        if (libreta["notes"] == undefined || libreta["notes"] == null){
          libreta["notes"] = [new Nota (Date.now(),Date.now(),nombre,"")];
        }else{
          libreta["notes"].push(new Nota (Date.now(),Date.now(),nombre,""));
        }
      }
    });
  }

  crearLibreta(nombre){
    this.libretas.push(new Libreta(Date.now(), Date.now(),nombre,[]));
  }

}
