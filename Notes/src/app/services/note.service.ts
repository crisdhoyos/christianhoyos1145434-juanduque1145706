import { Injectable } from '@angular/core';
import { config } from '../config';
import { Http, Headers } from '@angular/http';

@Injectable()
export class NoteService {
  result : any;
  constructor(private http:Http) { 
    this.result = document.getElementsByClassName("multi-files");
  }

  getNotes(query = null){
    let url = config.server + 'NotesServices/Note/getNote';
    url = (query == null) ? url : url+'/'+ query;
    return this.http.get(url);
  }

  addNote(title, date, content, notebook_id){
    let url = config.server + 'NotesServices/Note/insertNote';
    let headers  = new Headers();
    let json = {
      title: title,
      date: date,
      notebook_id: notebook_id,
      content: ''
    }
    var body = this.jsonToUrl(json);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(url,body,{headers : headers})
  
  }

  deleteNotes(id){
    let url = config.server + 'NotesServices/Note/deleteNote';
    let headers  = new Headers();
    let json = {
      id: id,
    }
    var body = this.jsonToUrl(json);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(url,body,{headers : headers})
  }


  jsonToUrl(json){
    return Object.keys(json).map(key =>encodeURIComponent(key) + '=' + encodeURIComponent(json[key])).join('&');
  }
  

  updateNotes(id, title, content){
    let url = config.server + 'NotesServices/Note/updateNote';
    let headers  = new Headers();
    let json = {
      id: id,
      title: title,
      content: content
    }
    var body = this.jsonToUrl(json);
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    return this.http.post(url,body,{headers : headers})
  }

}
