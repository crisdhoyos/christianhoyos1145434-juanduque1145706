import { TestBed, inject } from '@angular/core/testing';

import { NotebookService } from './notebook.service';

describe('NotebookService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NotebookService]
    });
  });

  it('should be created', inject([NotebookService], (service: NotebookService) => {
    expect(service).toBeTruthy();
  }));
});
