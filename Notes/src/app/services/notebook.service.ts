import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { config } from '../config';
import { NgForm } from '@angular/forms';

@Injectable()
export class NotebookService {

  constructor(private http : Http) { }
  
    getNotebooks(query : string = null){
      let url : string = config.server + 'NotesServices/Notebook/getNotebook';
      url = (query == null) ? url : url+'/'+ query;
      return this.http.get(url);
    }

    addNotebook(name, date, user_id){
      let url = config.server + 'NotesServices/Notebook/insertNotebook';
      let headers  = new Headers();
      let json = {
        name: name,
        date: date,
        user_id: user_id
      }
      var body = this.jsonToUrl(json);
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      return this.http.post(url,body,{headers : headers})
    
    }

    deleteNotebook(id){
      let url = config.server + 'NotesServices/Notebook/deleteNotebook';
      let headers  = new Headers();
      let json = {
        id: id,
      }
      var body = this.jsonToUrl(json);
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      return this.http.post(url,body,{headers : headers})
    }

    jsonToUrl(json){
      return Object.keys(json).map(key =>encodeURIComponent(key) + '=' + encodeURIComponent(json[key])).join('&');
    }

    updateNotebook(id, name){
      let url = config.server + 'NotesServices/Notebook/updateNotebook';
      let headers  = new Headers();
      let json = {
        id: id,
        name: name,
      }
      var body = this.jsonToUrl(json);
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      return this.http.post(url,body,{headers : headers})
    }

}
