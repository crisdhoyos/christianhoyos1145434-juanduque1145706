import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs';
import { NgForm } from '@angular/forms';
import { config } from '../config';
import { Router } from '@angular/router';

@Injectable()
export class UserService {

  serverUrl = config.server;
  currentBlob : any = null;

  constructor(private http: Http, private router : Router) { }
  addUser(form:  NgForm){
    var blob = null;
    let url = this.serverUrl + 'NotesServices/User/insertUser';
    if(this.currentBlob != null){
      let headers  = new Headers();
      console.dir(form.value);
      var body = this.jsonToUrl(form.value) + '&photo=' + this.currentBlob;
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      this.http.post(url,body,
      {
        headers : headers,
      })
      .subscribe(val => {
        console.log(val);
        this.router.navigate(['/signin']);
      });
    }
  }
  
  getUsers(query = null){
    let url = this.serverUrl + 'NotesServices/User/getUser';
    url = (query == null) ? url : url+'/'+ query;
    return this.http.get(url);
  }


  subeImagen(evt){
      let input = evt.target;
      if (input.files.length > 0) {
        let file = input.files[0];

        let imagePromise = this.getFileBlob(file);
        imagePromise.then(blob => {
            this.currentBlob = blob;
        });
      }
  }

  getFileBlob(file){
      var reader = new FileReader();
      return new Promise((resolve, reject) => {
          reader.onloadend = () => {
                  resolve(reader.result);
          };
          reader.readAsBinaryString(file);
      });
  }

  jsonToUrl(json){
    return Object.keys(json).map(key =>encodeURIComponent(key) + '=' + encodeURIComponent(json[key])).join('&');
  }

}
