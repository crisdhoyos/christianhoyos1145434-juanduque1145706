import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.scss']
})
export class SignInComponent implements OnInit, OnDestroy {

  constructor(private authService: AuthService, private router : Router) { 
    if(this.authService.getUserLogged()){
      this.router.navigate(['inicio']);
    } 
   }

  ngOnInit() {
  }
  ngOnDestroy(){
    
  }

  signIn(form : NgForm){
    let resultPromise = this.authService.signIn(form);
    resultPromise.then((res) => {

      if(res == 0){
        this.router.navigate(['inicio']);
      } else if(res == 1) {
  
      } else if(res == 2){
  
      }
      
    });
  }

}
