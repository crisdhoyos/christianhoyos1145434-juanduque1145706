<?php

class NoteController{
    public function index($name = null)
    {
        echo 'Este es el método por defecto index de NoteController';
    }
    public function insertNote(){
        header('Access-Control-Allow-Origin: http://localhost:4200'); 
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        if(isset($_POST)){
            require('./libs/connection.php');
            $title = $_POST['title'];
            $date = $_POST['date'];
            $notebook_id = $_POST['notebook_id'];
            $content = $_POST['content'];

            $sql = "INSERT INTO note (title, date, notebook_id, content)
            VALUES ('$title',$date,$notebook_id, '$content')";
            echo $sql;
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: ".$conn->error;
            }
            
            $conn->close();
        }
    }

    public function getNote($query = null){
        header('Access-Control-Allow-Origin: http://localhost:4200'); 
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        if(isset($_GET)){
            require('./libs/connection.php');

            $sql = "SELECT * FROM note";
            $sql = ($query != null) ? $sql.' WHERE '.$query : $sql;
            
            $result = $conn->query($sql);
            $array = array();
            if ($result->num_rows > 0) {
                $array[0] = 0;
                $array[1] = array();
                while($row = $result->fetch_assoc()) {
                    $array[1][] = $row;
                }
                echo json_encode($array);
            } else {
                $array[0] = 1;
                echo json_encode($array);
            }
            $conn->close();
        }
    }

    public function deleteNote(){
        header('Access-Control-Allow-Origin: http://localhost:4200'); 
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        if(isset($_POST)){
            require('./libs/connection.php');
            $id = $_POST['id'];

            $sql = "DELETE FROM note WHERE id =$id";
            echo $sql;
            
            if ($conn->query($sql) === TRUE) {
                echo "Note deleted successfully";
            } else {
                echo "Error: ".$conn->error;
            }        
            $conn->close();
        }
    }

    public function updateNote(){
        header('Access-Control-Allow-Origin: http://localhost:4200'); 
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        if(isset($_POST)){
            require('./libs/connection.php');
            $id = $_POST['id'];
            $title = $_POST['title'];
            $content = $_POST['content'];

            $sql = "UPDATE note SET title='$title', content='$content' WHERE id =$id";
            echo $sql;
            
            if ($conn->query($sql) === TRUE) {
                echo "Note deleted successfully";
            } else {
                echo "Error: ".$conn->error;
            }        
            $conn->close();
        }
    }


}