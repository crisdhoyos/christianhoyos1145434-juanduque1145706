<?php

class NotebookController{
    public function index($name = null)
    {
        echo 'Este es el método por defecto index de UserController';
    }
    public function insertNotebook(){
        header('Access-Control-Allow-Origin: http://localhost:4200'); 
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        if(isset($_POST)){
            require('./libs/connection.php');
            $name = $_POST['name'];
            $date = $_POST['date'];
            $user_id = $_POST['user_id'];

            $sql = "INSERT INTO notebook (name, date, user_id)
            VALUES ('$name',$date,$user_id)";
            echo $sql;
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: ".$conn->error;
            }
            
            $conn->close();
        }
    }

    public function getNotebook($query = null){
        header('Access-Control-Allow-Origin: http://localhost:4200'); 
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        if(isset($_GET)){
            require('./libs/connection.php');

            $sql = "SELECT * FROM notebook";
            $sql = ($query != null) ? $sql.' WHERE '.$query : $sql;
            
            $result = $conn->query($sql);
            $array = array();
            if ($result->num_rows > 0) {
                $array[0] = 0;
                $array[1] = array();
                while($row = $result->fetch_assoc()) {
                    $array[1][] = $row;
                }
                echo json_encode($array);
            } else {
                $array[0] = 1;
                echo json_encode($array);
            }
            $conn->close();
        }
    }

    public function deleteNotebook(){
        header('Access-Control-Allow-Origin: http://localhost:4200'); 
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        if(isset($_POST)){
            require('./libs/connection.php');
            $id = $_POST['id'];

            $sql = "DELETE FROM notebook WHERE id =$id";
            echo $sql;
            
            if ($conn->query($sql) === TRUE) {
                echo "New record deleted successfully";
            } else {
                echo "Error: ".$conn->error;
            }        
            $conn->close();
        }
    }

    public function updateNotebook(){
        header('Access-Control-Allow-Origin: http://localhost:4200'); 
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        if(isset($_POST)){
            require('./libs/connection.php');
            $id = $_POST['id'];
            $name = $_POST['name'];

            $sql = "UPDATE notebook SET name='$name' WHERE id =$id";
            
            if ($conn->query($sql) === TRUE) {
                echo "Notebook updated successfully";
            } else {
                echo "Error: ".$conn->error;
            }        
            $conn->close();
        }
    }
    
}