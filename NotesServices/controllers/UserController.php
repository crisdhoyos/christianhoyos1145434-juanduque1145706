<?php

class UserController{
    public function index($name = null)
    {
        echo 'Este es el método por defecto index de UserController';
    }
    public function insertUser(){
        header('Access-Control-Allow-Origin: http://localhost:4200'); 
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: POST');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        if(isset($_POST)){
            require('./libs/connection.php');
            $username = $_POST['username'];
            $email = $_POST['email'];
            $password = $_POST['password'];
            $photo = $_POST['photo'];

            $sql = "INSERT INTO user (username, email, password, photo)
            VALUES ('$username','$email','$password','$photo')";
            
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: ".$conn->error;
            }
            
            $conn->close();
        }
    }

    public function getUser($query = null){
        header('Access-Control-Allow-Origin: http://localhost:4200'); 
        header("Access-Control-Allow-Credentials: true");
        header('Access-Control-Allow-Methods: GET');
        header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization');
        if(isset($_GET)){
            require('./libs/connection.php');

            $sql = "SELECT * FROM user";
            $sql = ($query != null) ? $sql.' WHERE username="'.$query.'"' : $sql;
            
            $result = $conn->query($sql);
            $array = array();
            if ($result->num_rows > 0) {
                $array[0] = 0;
                $array[1] = array();
                while($row = $result->fetch_assoc()) {
                    $array[1][] = $row;
                }
                echo json_encode($array);
            } else {
                $array[0] = 1;
                echo json_encode($array);
            }
            $conn->close();
        }
    }


}