<?php
class Video{
    private $nombre;
    private $description;
    private $src;

    function __construct($name, $description){
        $this->$name = $name;
        $this->$description = $description;
        //$this->$name = $name;
    }

    function getName(){
        return  $this->$name;
    }

    function getDescription(){
        return  $this->$description;
    }

    function setName($val){
        $this->$name = $val;
    }

    function setDescription($val){
        $this->$description = $val;
    }

    function toArray(){
        return get_object_vars($this);
    }

}