# Parcial 2 Elect. Desarrollo Web 2017-2
##### Profesor: Pablo Bejarano
##### Fecha de presentación y entrega: Lunes 23 de Octubre de 2017
##### Duración: 2h 30m
##### El examen parcial equivale al 100% del corte
#
#
El examen será elaborado en parejas, cada pareja realizará un [fork](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html) del repositorio base y entregará su parcial como un proyecto privado de Bitbucket en el cual incluirán al docente como miembro administrador del proyecto.


## Desarrollo

### La idea - ¿Qué vamos a hacer?

Ustedes desarrollarán una aplicación tanto en su frontend como su backend utilizando angular 4 cómo tecnología para el cliente y PHP como tecnología para el backend. Construiran una aplicación para crear Notas al estilo Apple Notes (Notas.app).

## Funcionalidades de la aplicación

Las funcionalidades:

1. **Bronce** son aquellas que deben estár en el producto final para poder ser viable, si alguna funcionalidad bronce no está, el producto no es calificable.
2. **Plata** son funcionalidades que suman puntos al proyecto y por las cuales usted podra sumer hasta 5 puntos en la parte teórica.
3. **Oro** son funcionalidades bonus que usted podrá hacer de manera opcional, lo que significa que no hacerlas no hará que su parte práctica tenga una nota inferior, pero realizarlas otorgará bonificaciones a la calificación ganada por puntos plata.

**Nota:** Las funcionalidades valen 0, 0.5 ó 1, es decir, nada, la mitad o toda.

### Piano Online

#### Bronce

1. Los usuarios podrán **registrarse** en la aplicación.
2. La aplicación está desarrollada con **Angular 4.**
3. La información es alojada en **MariaDB / MySQL** usando un backend **PHP**.
4. El Backend responde siempre en formato **JSON**.
5. La aplicación consulta información mediante **Services** de **Angular**.
6. La aplicación será desarrollada orientada a objetos respetando la arquitectura del framework, por ende cada **entidad** será creada como una **clase** y alojada en una carpeta **"models"**.

### Plata

1. Los usuarios podrán crear, editar y eliminar nuevas **libretas**.
2. Los Usuarios podrán crear, editar y eliminar nuevas **notas**. Las notas deben ser guardadas con fecha de edición en la base de datos y sus campos son: título, cuerpo y fecha.
3. Los usuarios podrán acceder a **sus libretas** y **notas** (Ojo, las de ellos).
4. Las notas se **guardan automáticamente** mientras el usuario escribe en ellas.

Nota: Toda la funcionalidad está explicada en el siguiente [video](./Demo.mp4). Las notas se gestionan igual que las libretas.

### Oro

1. Las notas se ordenan en orden de edición (la más reciente primero). (+0.5)
2. Los usuarios podrán compartir notas entre ellos, y así mismo gestionarlas juntos. (+0.5)

## Entrega

Ustedes sustentarán su proyecto en sus computadores y este será revisado desde el último commit al repositorio (si su último commit se da depsués de las 5:00 pm, se revisará el commit en horario válido más cercano).

Los proyectos deben ser sustentados entre las 5:00pm y las 5:30pm para la calificación in situ.

Si no se sustenta en la franja establecida su nota será equivalente a una entrega en blanco.

Muchos exitos.

# Recuerde

Usted tiene acceso a todo su material de curso, internet y código previo. Está prohibido hablar con otros grupos o usar redes sociales.