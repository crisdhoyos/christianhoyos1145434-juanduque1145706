-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema parcial2web1
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema parcial2web1
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `parcial2web1` DEFAULT CHARACTER SET utf8 ;
USE `parcial2web1` ;

-- -----------------------------------------------------
-- Table `parcial2web1`.`USUARIO`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parcial2web1`.`USUARIO` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `photo` LONGBLOB NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `parcial2web1`.`LIBRETA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parcial2web1`.`LIBRETA` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fecha` INT NOT NULL,
  `nombre` VARCHAR(45) NOT NULL,
  `USUARIO_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_LIBRETA_USUARIO_idx` (`USUARIO_id` ASC),
  CONSTRAINT `fk_LIBRETA_USUARIO`
    FOREIGN KEY (`USUARIO_id`)
    REFERENCES `parcial2web1`.`USUARIO` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `parcial2web1`.`NOTA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parcial2web1`.`NOTA` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `fecha` INT NOT NULL,
  `titulo` VARCHAR(45) NOT NULL,
  `contenido` LONGTEXT NULL,
  `LIBRETA_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_NOTAS_LIBRETA1_idx` (`LIBRETA_id` ASC),
  CONSTRAINT `fk_NOTAS_LIBRETA1`
    FOREIGN KEY (`LIBRETA_id`)
    REFERENCES `parcial2web1`.`LIBRETA` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `parcial2web1`.`LIBRETA_COMPARTIDA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parcial2web1`.`LIBRETA_COMPARTIDA` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `permisos` INT NOT NULL,
  `USUARIO_id` INT NOT NULL,
  `LIBRETA_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_LIBRETA_COMPARTIDA_USUARIO1_idx` (`USUARIO_id` ASC),
  INDEX `fk_LIBRETA_COMPARTIDA_LIBRETA1_idx` (`LIBRETA_id` ASC),
  CONSTRAINT `fk_LIBRETA_COMPARTIDA_USUARIO1`
    FOREIGN KEY (`USUARIO_id`)
    REFERENCES `parcial2web1`.`USUARIO` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_LIBRETA_COMPARTIDA_LIBRETA1`
    FOREIGN KEY (`LIBRETA_id`)
    REFERENCES `parcial2web1`.`LIBRETA` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `parcial2web1`.`NOTA_COMPARTIDA`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `parcial2web1`.`NOTA_COMPARTIDA` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `permisos` INT NOT NULL,
  `NOTA_id` INT NOT NULL,
  `USUARIO_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_NOTA_COMPARTIDA_NOTA1_idx` (`NOTA_id` ASC),
  INDEX `fk_NOTA_COMPARTIDA_USUARIO1_idx` (`USUARIO_id` ASC),
  CONSTRAINT `fk_NOTA_COMPARTIDA_NOTA1`
    FOREIGN KEY (`NOTA_id`)
    REFERENCES `parcial2web1`.`NOTA` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_NOTA_COMPARTIDA_USUARIO1`
    FOREIGN KEY (`USUARIO_id`)
    REFERENCES `parcial2web1`.`USUARIO` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
